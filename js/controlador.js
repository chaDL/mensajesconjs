function alertFun() {
    alert('¡Mensaje de Alerta!');
}

function confirmFun() {
    var mensaje;

    if ( confirm("Presiona el boton aceptar") ) {
        mensaje = "Gracias por presionar el boton";
    } else {
        mensaje = "Cancelaste la accion";
    }

    document.getElementById("actionAlert").innerHTML = mensaje;
}

function promptFun() {
    var segundoMensaje;
    var nombre = prompt("Ingresa tu nombre: ", "Erick");

    if ( nombre == null || nombre == "" ) {
        segundoMensaje = "¡No escribiste tu nombre! :("
    } else {
        segundoMensaje = "Tu nombre " + nombre + "es feo.";
    }

    document.getElementById("actionAlert").innerHTML = segundoMensaje;
}